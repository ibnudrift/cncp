
<div class="backs_brown_trees_bottom_pg py-5">
    <div class="prelatife container">
        <div class="inners text-center bxs_text d-block mx-auto">
            <?php if (Yii::app()->language == 'en'): ?>
            <h5><b>Get In Touch</b> With CNCP</h5>
            <?php else: ?>
            <h5><b>Hubungi</b> CNCP</h5>
            <?php endif; ?>
            <div class="py-2"></div>
            <div class="row">
                <div class="col-md-30 br_right">
                    <div class="box_text py-2">
                        <span>Hotline / Whatsapp Chat</span>
                        <a href="https://wa.me/628123556550">081 2355 6550</a>
                    </div>
                </div>
                <div class="col-md-30">
                    <div class="box_text py-2">
                        <span>Email</span>
                        <a href="mailto:info@cncpwod.com">info@cncpwod.com</a>
                    </div>
                </div>
            </div>
            <div class="clearfix clear"></div>
        </div>
    </div>
</div>

<footer class="foot pt-4 pb-3">
    <div class="container_foot px-5">
        <div class="inners py-0">
            <div class="row">
                <div class="col-md-30">
                     <div class="lgo_footers"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>"><img src="<?php echo $this->assetBaseurl; ?>logo-footer.png" alt="" class="img img-fluid"></a></div>
                     <div class="py-2"></div>
                     <address class="n_foots">
                         Geger Wetan, Ikerikergeger, Cerme, Gresik Regency, East Java 61171 <br>
                         Tel. (031) 79973739
                     </address>
                </div>
                <div class="col-md-30">
                    <div class="t-copyrights ptop text-right">
                        Copyright &copy; 2020 - PT. Cahaya Niaga Citra Persada. All rights reserved.
                    </div>
                    <div class="py-1"></div>
                    <div class="t-copyrights text-right">
                        <p>Website design by <a target="_blank" title="Website Design Surabaya" href="https://markdesign.net">Mark Design Jakarta.</a> Website design & development Jakarta.</p>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</footer>

<section class="live-chat">
    <div class="row">
        <div class="col-md-60">
            <div class="live">
                <a href="https://wa.me/628123556550">
                    <img src="<?php echo $this->assetBaseurl; ?>icon-whatsapp-float.svg" class="img img-fluid" alt="">
                </a>
            </div>
        </div>
    </div>
</section>

<section class="nfoot_mob_wa d-block d-sm-none">
    <div class="row">
        <div class="col-md-60">
            <div class="nx_button">
                <a href="https://wa.me/628123556550"><i class="fa fa-whatsapp"></i> Whatsapp</a>
            </div>
        </div>
    </div>
</section>
<style type="text/css">
    .imgs_vold img{
        max-width: 255px;
    }
    .live-chat .live img{
        max-width: 175px;
    }
</style>