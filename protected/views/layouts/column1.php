<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<?php echo $this->renderPartial('//layouts/_header', array()); ?>

<?php 
// get slides
$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('active = "1"');
$criteria->addCondition('description.language_id = :language_id');
// $criteria->params[':language_id'] = $this->languageID;
$criteria->params[':language_id'] = 2;
$criteria->order = 't.urutan ASC';
$slides = Slide::model()->findAll($criteria);
?>
<div class="fcs-wrapper outers_fcs_wrapper prelatife wrapper-slide">
    <div class="container cont-fcs">
        <div id="myCarousel_home" class="carousel slide" data-ride="carousel" data-interval="4500">
            <div class="carousel-inner">

                <?php foreach ($slides as $keys => $value): ?>
                <div class="carousel-item <?php if ($keys == 0): ?>active<?php endif ?> home-slider-new">
                    <img class="fcs_dekstop w-100 d-none d-sm-block" src="<?php echo Yii::app()->baseUrl.'/images/slide/'. $value->image ?>" alt="First slide" style="background-repeat: no-repeat; background-size: cover;">
                    <img class="fcs_mob d-block d-sm-none img img-fluid" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1080,2022, '/images/slide/'. $value->image2, array('method' => 'adaptiveResize', 'quality' => '90')); ?>" alt="">
                    <div class="carousel-caption caption-slider-home">
                        <div class="prelative container">
                            <div class="bxsl_tx_fcs">
                                <div class="row no-gutters">
                                    <div class="col-md-60 text-left">
                                        <?php echo Tt::t('front', 't_fcs') ?>
                                        <div class="py-2"></div>
                                        <div class="blocks_bn_bottoms_red">
                                            <div class="btns blc1 d-inline-block align-middle">
                                                <a href="<?php echo CHtml::normalizeUrl(array('/home/about', 'lang'=>Yii::app()->language)); ?>" class="btn btn-link btns_bdefaults"><?php echo Tt::t('front', 'GET TO KNOW US') ?></a>
                                            </div>
                                            <div class="btns blc2 d-inline-block align-middle">
                                                <a href="<?php echo CHtml::normalizeUrl(array('/home/products', 'lang'=>Yii::app()->language)); ?>" class="btn btn-link btns_bdefaults"><?php echo Tt::t('front', 'OUR SERVICES') ?></a>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach ?>

            </div>


           <div class="carousel-button-native">
                <div class="">
                    <ol class="carousel-indicators">
                        <?php foreach ($slides as $keys => $value): ?>
                        <li data-target="#myCarousel_home" data-slide-to="<?php echo $keys ?>" class="<?php echo ($keys == 0)? "active":"" ?>"></li>
                        <?php endforeach ?>
                    </ol>
                </div>
            </div>
        </div>
        
    </div>
</div>

<?php echo $content ?>

<script type="text/javascript">
    $(document).ready(function(){
        
        if ($(window).width() > 768) {
            var $item = $('#myCarousel_home.carousel .carousel-item'); 
            var $wHeight = $(window).height();
            $item.eq(0).addClass('active');
            $item.height($wHeight); 
            $item.addClass('full-screen');

            $('#myCarousel_home.carousel img.fcs_dekstop').each(function() {
              var $src = $(this).attr('src');
              var $color = $(this).attr('data-color');
              $(this).parent().css({
                'background-image' : 'url(' + $src + ')',
                'background-color' : $color
              });
              $(this).remove();
            });

            $(window).on('resize', function (){
              $wHeight = $(window).height();
              $item.height($wHeight);
            });

            $('#myCarousel_home.carousel').carousel({
              interval: 4500,
              pause: "false"
            });
        }

    });
</script>

<?php echo $this->renderPartial('//layouts/_footer', array()); ?>

<?php 
    $e_activemenu = $this->action->id;
    $controllers_ac = $this->id;

    $active_menu_pg = $controllers_ac.'/'.$e_activemenu;
?>

<?php if ($active_menu_pg == 'home/index'): ?>
<style type="text/css">
    @media screen and (max-width: 600px){
        body {
            padding-top: 0px;
        }
    }
</style>
<?php endif ?>

<?php $this->endContent(); ?>
