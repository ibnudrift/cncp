<?php 
$criteria = new CDbCriteria;
$criteria->addCondition('active = "1"');
$criteria->order = 't.date_input ASC';
$mod_kategh = ViewGallery::model()->findAll($criteria);
?>

<header class="head headers fixed-top <?php if ($active_menu_pg == 'home/index' or $active_menu_pg == 'home/abouthistory' or $active_menu_pg == 'home/aboutquality' or $active_menu_pg == 'home/aboutcareer'): ?>homes_head<?php endif ?> ">
  <div class="prelative container d-none d-sm-block">
    <div class="">
      <div class="row">
        <div class="col-md-20 col-lg-20">
          <div class="logo_heads">
            <div class="d-inline-block align-middle">
              <a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">
                <img src="<?php echo $this->assetBaseurl; ?>lgo-headers.png" alt="<?php echo Yii::app()->name; ?>" class="img img-fluid">
              </a>
            </div>

          </div>
        </div>

        <div class="col-md-40 col-lg-40">
          <div class="texts_languages text-right">
            <?php
            $get = $_GET;
            $get['lang'] = 'en';
            ?>
            <a href="<?php echo $this->createUrl($this->route, $get) ?>">EN</a>
            &nbsp;|&nbsp;
            <?php
            $get = $_GET;
            $get['lang'] = 'id';
            ?>
            <a href="<?php echo $this->createUrl($this->route, $get) ?>">ID</a>
          </div>
          <div class="py-0 d-block"></div>
          <div class="text-right top-menu">
            <ul class="list-inline">
              <li class="list-inline-item">
                <a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>"><?php echo Tt::t('front', 'HOME') ?></a>
              </li>
              <li class="list-inline-item">
                <a href="<?php echo CHtml::normalizeUrl(array('/home/about', 'lang'=>Yii::app()->language)); ?>"><?php echo Tt::t('front', 'OUR COMPANY') ?></a>
              </li>
              <li class="list-inline-item">
                <a href="<?php echo CHtml::normalizeUrl(array('/home/products', 'lang'=>Yii::app()->language)); ?>"><?php echo Tt::t('front', 'PRODUCTS & SERVICES') ?></a>
              </li>
              <li class="list-inline-item">
                <a href="<?php echo CHtml::normalizeUrl(array('/home/quality', 'lang'=>Yii::app()->language)); ?>"><?php echo Tt::t('front', 'QUALITY PROCESS') ?></a>
              </li>
              <li class="list-inline-item">
                <a href="<?php echo CHtml::normalizeUrl(array('/home/contact', 'lang'=>Yii::app()->language)); ?>"><?php echo Tt::t('front', 'CONTACT') ?></a>
              </li>
              <li class="list-inline-item">
                <a href="https://wa.me/628123556550"><img src="<?php echo $this->assetBaseurl; ?>icon-lg-wa.png" alt="" class="img img-fluid"></a>
              </li>
            </ul>
          </div>
          <div class="clear clearfix"></div>
        </div>
      </div>
      <div class="clear"></div>
    </div>

  </div>
  
  <div class="d-block d-sm-none">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">
        <img src="<?php echo $this->assetBaseurl.'logo-footer.png' ?>" alt="<?php echo Yii::app()->name; ?>" class="img img-fluid">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>">HOME</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/about', 'lang'=>Yii::app()->language)); ?>">OUR COMPANY</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/products', 'lang'=>Yii::app()->language)); ?>">PRODUCTS & SERVICES</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/quality', 'lang'=>Yii::app()->language)); ?>">QUALITY PROCESS</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/contact', 'lang'=>Yii::app()->language)); ?>">CONTACT</a>
          </li>
        </ul>
        <div class="py-1"></div>
        <div class="texts_languages text-left">
            <?php
            $get = $_GET;
            $get['lang'] = 'en';
            ?>
            <a href="<?php echo $this->createUrl($this->route, $get) ?>">EN</a>
            &nbsp;|&nbsp;
            <?php
            $get = $_GET;
            $get['lang'] = 'id';
            ?>
            <a href="<?php echo $this->createUrl($this->route, $get) ?>">ID</a>
          </div>
      </div>
    </nav>
  </div>

</header>
