<section class="insidespg-cover py-5" style="background-image: url('<?php echo $this->assetBaseurl ?>cover-products.jpg');">
    <div class="outers_block_inner">
        <div class="prelative container">
            <div class="row">
                <div class="col-md-60">
                    <div class="insides_intext">
                        <h3 class="mb-2"><?php echo Tt::t('front', 'PRODUCTS & SERVICES') ?></h3>
                        <div class="py-1"></div>
                        <p><?php echo Tt::t('front', 'Wood Manufacturing Range Of Production') ?></p>
                        <div class="clear"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<section class="product-sec-1 py-5 back-white">
    <div class="prelative container py-5">
        <div class="py-3"></div>
        <div class="row text-center py-4">
            <div class="col-md-60 content-text text-center">
                <?php if (Yii::app()->language == 'en'): ?>
                    <h4>CNCP PRODUCTS & SERVICES</h4>
                    <h3>We Offer The Comprehensive Range Of Products & Services Beyond <br>Wood Manufacturing & Wood Working Standards</h3>
                <?php else: ?>
                    <h4>PRODUK & LAYANAN CNCP</h4>
                    <h3>Kami Menawarkan Rangkaian Produk & Layanan Yang Lengkap Di Luar <br>Standar Manufaktur Kayu & Standar Pengerjaan Kayu</h3>
                <?php endif ?>
                <div class="py-3"></div>
                <?php 
                $data_products = [
                                    [
                                        'pict'=>'product-banners-1.jpg',
                                        'title'=>'Saw Mill Services',
                                        'desc'=>'At CNCP, we process your logs into valuable lumber by operating our various capacity & sizes of saw mill machine according to the log size.',
                                        'title_id'=>'Layanan Saw Mill',
                                        'desc_id'=>'Di CNCP, kami memproses log Anda menjadi kayu yang berharga dengan mengoperasikan berbagai kapasitas & ukuran mesin saw mill kami sesuai dengan ukuran log.',
                                    ],
                                    [
                                        'pict'=>'product-banners-2.jpg',
                                        'title'=>'Kiln Dry Services',
                                        'desc'=>'CNCP wood factory are equipped with 17 Kiln units and offer custom kiln-drying as a service to our customers.',
                                        'title_id'=>'Layanan Kiln Dry',
                                        'desc_id'=>'Pabrik kayu CNCP dilengkapi dengan 17 unit Kiln dan menawarkan pengeringan kiln khusus sebagai layanan kepada pelanggan kami.',
                                    ],
                                    [
                                        'pict'=>'product-banners-3.jpg',
                                        'title'=>'Wood Working Services',
                                        'desc'=>'Custom carpentry wood as per request by our customers to support architectural, interior furnishing and engineering needs as a semi finished and as well as finished materials supply.',
                                        'title_id'=>'Jasa Pengerjaan Kayu',
                                        'desc_id'=>'Kayu pertukangan khusus sesuai permintaan pelanggan kami untuk mendukung kebutuhan arsitektural, perabotan interior dan teknik sebagai pasokan bahan setengah jadi dan juga bahan jadi.',
                                    ],
                                    
                                ];
                ?>
                <div class="lists_products_nservice">
                    <div class="row">
                        <?php foreach ($data_products as $key => $value): ?>
                        <div class="col-md-20">
                            <div class="items">
                                <div class="picture"><img src="<?php echo $this->assetBaseurl ?><?php echo $value['pict'] ?>" alt="" class="img img-fluid"></div>
                                <div class="info">
                                    <?php if (Yii::app()->language == 'en'): ?>
                                        <h4><?php echo $value['title'] ?></h4>
                                        <p><?php echo $value['desc'] ?></p>
                                    <?php else: ?>
                                        <h4><?php echo $value['title_id'] ?></h4>
                                        <p><?php echo $value['desc_id'] ?></p>
                                    <?php endif ?>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach ?>
                    </div>
                </div>
                <!-- End lists product -->

                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>

<section class="product-sec-2 py-5 back-white">
    <div class="prelative container py-5">
        <div class="row text-center">
            <div class="col-md-60 text-center">

                <div class="tops_info_section">
                    <h4 class="small"><?php echo Tt::t('front', 'PRODUCT REFERENCES') ?></h4>
                </div> 
                <div class="py-2"></div>

                <?php 
                $data_products = [
                                    [
                                        'pict'=>'product-refn-1.jpg',
                                        'title'=>'Merbau Wood Products',
                                        'title_id'=>'Produk Kayu Merbau',
                                    ],
                                    [
                                        'pict'=>'product-refn-2.jpg',
                                        'title'=>'Balsa Wood Products',
                                        'title_id'=>'Produk Kayu Balsa',
                                    ],
                                    [
                                        'pict'=>'product-refn-3.jpg',
                                        'title'=>'Sengon Wood Products',
                                        'title_id'=>'Produk Kayu Sengon',
                                    ],
                                    [
                                        'pict'=>'product-refn-4.jpg',
                                        'title'=>'Meranti Wood Products',
                                        'title_id'=>'Produk Kayu Meranti',
                                    ],

                                    [
                                        'pict'=>'product-refn-5.jpg',
                                        'title'=>'Teak Wood Products',
                                        'title_id'=>'Produk Kayu Jati',
                                    ],
                                    [
                                        'pict'=>'product-refn-6.jpg',
                                        'title'=>'Lingua Wood Products',
                                        'title_id'=>'Produk Kayu Lingua',
                                    ],
                                    [
                                        'pict'=>'product-refn-7.jpg',
                                        'title'=>'Other Wood Products',
                                        'title_id'=>'Produk Kayu Lainnya',
                                    ],
                                    
                                ];
                ?>
                <div class="lists_products_nreferences">
                    <div class="row justify-content-center">
                        <?php foreach ($data_products as $key => $value): ?>
                        <div class="col-md-15">
                            <div class="items">
                                <div class="picture"><img src="<?php echo $this->assetBaseurl ?><?php echo $value['pict'] ?>" alt="" class="img img-fluid"></div>
                                <div class="info">
                                    <?php if (Yii::app()->language == 'en'): ?>
                                        <h4><?php echo $value['title'] ?></h4>
                                    <?php else: ?>
                                        <h4><?php echo $value['title_id'] ?></h4>
                                    <?php endif ?>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach ?>
                    </div>
                </div>
                <!-- End lists services -->
                
                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>

<section class="about-sec-3 py-5 back-red blocks_outer_vision_mission">
    <div class="prelative container py-5 text-center">
        <div class="inners">
            <div class="tops_info_section">
                <h4 class="small mb-0"><?php echo Tt::t('front', 'OUR MARKETS') ?></h4>
            </div> 
            <div class="py-0"></div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="picts_full px-5 pb-4">
        <img src="<?php echo $this->assetBaseurl ?>maps_about_company.jpg" alt="" class="img img-fluid">
    </div>
</section>