<section class="insidespg-cover py-5" style="background-image: url('<?php echo $this->assetBaseurl ?>cover-qualitys.jpg');">
    <div class="outers_block_inner">
        <div class="prelative container">
            <div class="row">
                <div class="col-md-60">
                    <div class="insides_intext">
                        <h3 class="mb-2"><?php echo Tt::t('front', 'OUR QUALITY') ?></h3>
                        <div class="py-1"></div>
                        <?php if (Yii::app()->language == 'en'): ?>
                            <p>The Distance We Go For Perfections</p>
                        <?php else: ?>
                            <p>Jarak Yang Kami Pergi Untuk Kesempurnaan</p>
                        <?php endif ?>
                        <div class="clear"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<section class="quality-sec-1 py-5 back-white">
    <div class="prelative container py-5">
        <div class="py-3"></div>
        <div class="row text-center py-4">
            <div class="col-md-60 content-text text-center">
                <?php if (Yii::app()->language == 'en'): ?>
                    <h4>CNCP PRODUCT & SERVICE QUALITY STATEMENT</h4>
                    <h3>Your Success Is Our First Priority</h3>
                    <p>We are committed to providing high quality precision wood commodity that meet customer needs and requirements. We recognize the quality of our wood working and manufacturing products - and we knew that it will have a powerful impact on our customers operations and reputation. Therefore, we continuously improve our day-to-day operations and quality checking in order to deliver flawless finished and semi finished wood commodity products that meet the quality expectations of our customers. Our up experienced quality control team is one of the most essential part of our company.</p>
                    <p><b>Our Product Quality Features:</b></p>
                <?php else: ?>
                    <h4>PERNYATAAN KUALITAS PRODUK & LAYANAN CNCP</h4>
                    <h3>Kesuksesan Anda Adalah Prioritas Pertama Kami</h3>
                    <p>Kami berkomitmen untuk menyediakan komoditas kayu presisi berkualitas tinggi yang memenuhi kebutuhan dan persyaratan pelanggan. Kami mengakui kualitas pengerjaan kayu dan produk manufaktur kami - dan kami tahu bahwa hal itu akan berdampak kuat pada operasi dan reputasi pelanggan kami. Oleh karena itu, kami terus meningkatkan operasi sehari-hari dan pemeriksaan kualitas untuk menghasilkan produk komoditas kayu jadi dan setengah jadi tanpa cacat yang memenuhi harapan kualitas pelanggan kami. Tim kontrol kualitas kami yang berpengalaman adalah salah satu bagian terpenting dari perusahaan kami.</p>
                    <p><b>Fitur Kualitas Produk Kami:</b></p>
                <?php endif ?>

                <div class="py-3"></div>
                <?php 
                $data_products = [
                                    [
                                        'pict'=>'bcsx_icon_dnyears-1.jpg',
                                        'title'=>'Legal & Qualified Wood Material Sourcing',
                                        'title_id'=>'Sumber Bahan Kayu yang Legal & Berkualitas',
                                    ],
                                    [
                                        'pict'=>'bcsx_icon_dnyears-2.jpg',
                                        'title'=>'High Precision & High Durability',
                                        'title_id'=>'Presisi Tinggi & Daya Tahan Tinggi',
                                    ],
                                    [
                                        'pict'=>'bcsx_icon_dnyears-3.jpg',
                                        'title'=>'Tested & Matched To Customer’s Request',
                                        'title_id'=>'Diuji & Cocok dengan Permintaan Pelanggan',
                                    ],
                                ];
                ?>
                <div class="lists_qualitys_icon">
                    <div class="row">
                        <?php foreach ($data_products as $key => $value): ?>
                        <div class="col-md-20">
                            <div class="items">
                                <div class="picture"><img src="<?php echo $this->assetBaseurl ?><?php echo $value['pict'] ?>" alt="" class="img img-fluid"></div>
                                <div class="info">
                                    <?php if (Yii::app()->language == 'en'): ?>
                                        <h4><?php echo $value['title'] ?></h4>
                                    <?php else: ?>
                                        <h4><?php echo $value['title_id'] ?></h4>
                                    <?php endif ?>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach ?>
                    </div>
                </div>
                <!-- End lists product -->

                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>

<section class="quality-sec-2 py-5 back-white">
    <div class="prelative container py-5">
        <div class="row text-center py-4">
            <div class="col-md-60 text-center">

                <div class="tops_info_section content-text">
                    <?php if (Yii::app()->language == 'en'): ?>
                        <h4 class="small">OUR PROCESS</h4>
                        <h3>Quality On Every Steps</h3>
                    <?php else: ?>
                        <h4 class="small">PROSES KAMI</h4>
                        <h3>Kualitas Di Setiap Langkah</h3>
                    <?php endif ?>
                </div> 

                <div class="py-2"></div>

                <?php 
                $data_p_qualitys = [
                                    [
                                        'pict'=>'banners_loc_item_quality_1.jpg',
                                        'title'=>'Legal & Qualified Wood Material Sourcing',
                                        'title_id'=>'Sumber Bahan Kayu yang Legal & Berkualitas',
                                    ],
                                    [
                                        'pict'=>'banners_loc_item_quality_2.jpg',
                                        'title'=>'Wood Log Processing With Bandsaw',
                                        'title_id'=>'Pengolahan Kayu Log Dengan Gergaji Besi',
                                    ],
                                    [
                                        'pict'=>'banners_loc_item_quality_3.jpg',
                                        'title'=>'Accustomed With Efficiency In Management & Organizing',
                                        'title_id'=>'Terbiasa Dengan Efisiensi Dalam Manajemen & Pengorganisasian',
                                    ],
                                    [
                                        'pict'=>'banners_loc_item_quality_4.jpg',
                                        'title'=>'17 Kiln Dry Chambers With High Precision Control Center',
                                        'title_id'=>'17 Kamar Kiln Kering Dengan Pusat Kontrol Presisi Tinggi',
                                    ],
                                    [
                                        'pict'=>'banners_loc_item_quality_5.jpg',
                                        'title'=>'High Speed Planner Machines',
                                        'title_id'=>'Mesin Perencana Kecepatan Tinggi',
                                    ],
                                    [
                                        'pict'=>'banners_loc_item_quality_6.jpg',
                                        'title'=>'Rip Machines To Divide Timbers Parallel To The Grain',
                                        'title_id'=>'Mesin Rip Untuk Membagi Timbers Sejajar Dengan Butir',
                                    ],
                                    [
                                        'pict'=>'banners_loc_item_quality_7.jpg',
                                        'title'=>'Cross Cut Process To Match Customer’s Requirements',
                                        'title_id'=>'Proses Lintas Potong Untuk Mencocokkan Kebutuhan Pelanggan',
                                    ],
                                    [
                                        'pict'=>'banners_loc_item_quality_8.jpg',
                                        'title'=>'High Efficiency Moulding Machines',
                                        'title_id'=>'Mesin Cetak Efisiensi Tinggi',
                                    ],
                                    [
                                        'pict'=>'banners_loc_item_quality_9.jpg',
                                        'title'=>'Tooling Workshop To Sharpen Saws & Cutter Heads',
                                        'title_id'=>'Bengkel Perkakas Untuk Mengasah Kepala Gergaji & Pemotong',
                                    ],
                                    [
                                        'pict'=>'banners_loc_item_quality_10.jpg',
                                        'title'=>'Neat & Tidy Storage Warehouses',
                                        'title_id'=>'Gudang Penyimpanan Rapi & Rapi',
                                    ],
                                    [
                                        'pict'=>'banners_loc_item_quality_11.jpg',
                                        'title'=>'Wood Products Hand Repairing For Final Quality Checking',
                                        'title_id'=>'Perbaikan Tangan Produk Kayu Untuk Pemeriksaan Kualitas Akhir',
                                    ],
                                    [
                                        'pict'=>'banners_loc_item_quality_12.jpg',
                                        'title'=>'Elaborated Load & Dispatch Procedure For Export',
                                        'title_id'=>'Prosedur Pengiriman & Pengiriman yang Dijelaskan Untuk Ekspor',
                                    ]
                                ];
                ?>
                <div class="lists_products_nreferences">
                    <div class="row">
                        <?php foreach ($data_p_qualitys as $key => $value): ?>
                        <div class="col-md-20">
                            <div class="items">
                                <div class="picture"><img src="<?php echo $this->assetBaseurl ?>quality/<?php echo $value['pict'] ?>" alt="" class="img img-fluid"></div>
                                <div class="info">
                                    <?php if (Yii::app()->language == 'en'): ?>
                                        <h4><?php echo $value['title'] ?></h4>
                                    <?php else: ?>
                                        <h4><?php echo $value['title_id'] ?></h4>
                                    <?php endif ?>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach ?>
                    </div>
                </div>
                <!-- End lists services -->
                
                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>
