<section class="insidespg-cover py-5" style="background-image: url('<?php echo $this->assetBaseurl ?>cover-contacts.jpg');">
    <div class="outers_block_inner">
        <div class="prelative container">
            <div class="row">
                <div class="col-md-60">
                    <div class="insides_intext">
                        <h3 class="mb-2"><?php echo Tt::t('front', 'CONTACT') ?></h3>
                        <div class="py-1"></div>
                        <?php if (Yii::app()->language == 'en'): ?>
                          <p>We’re Here To Help You</p>
                        <?php else: ?>
                          <p>Kami Di Sini Untuk Membantu Anda</p>
                        <?php endif ?>
                        <div class="clear"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<section class="contact-sec-1 py-5 back-white">
    <div class="prelative container py-5">
        <div class="py-3"></div>
        
          <div class="row blocks_info_contact">
            <div class="col-md-30 my-auto">
              <div class="picture"><img src="<?php echo $this->assetBaseurl ?>banner-contact-1.jpg" alt="" class="img img-fluid w-100"></div>
            </div>
            <div class="col-md-30 my-auto">
              <div class="info px-3">
                <h4><?php echo Tt::t('front', 'HEAD OFFICE & FACTORY') ?></h4>
                <p>Geger Wetan, Ikerikergeger, Kec. Cerme,<br>
                  Kabupaten Gresik, Jawa Timur 61171<br>
                  INDONESIA <br>
                  <a href="https://maps.app.goo.gl/vhpm72cY8cmpdD49A" class="cl_map"><?php echo Tt::t('front', 'View On Google Map') ?></a></p>
                <p><b><?php echo Tt::t('front', 'Telephone') ?>.</b> <br>
                (031) 79973739</p>
                <div class="clear"></div>
              </div>
            </div>
          </div>

          <div class="py-4"></div>

          <div class="row blocks_info_contact">
            <div class="col-md-30 my-auto order-2 order-sm-1">
              <div class="info px-3 text-right">
                <h4><?php echo Tt::t('front', 'CNCP INQUIRY CONTACT') ?></h4>
                <p><b><?php echo Tt::t('front', 'Hotline') ?> / Whatsapp Chat</b> <br>
                <a href="https://wa.me/628123556550">+62 81 2355 6550</a></p>
                <p><b>Email</b> <br><a href="mailto:info@cncpwood.com">info@cncpwood.com</a></p>
                <div class="clear"></div>
              </div>
            </div>
            <div class="col-md-30 my-auto order-1 order-sm-2">
              <div class="picture"><img src="<?php echo $this->assetBaseurl ?>banner-contact-2.jpg" alt="" class="img img-fluid w-100"></div>
            </div>
          </div>

          <div class="py-3"></div>

        <div class="clear clearfix"></div>
    </div>
</section>
