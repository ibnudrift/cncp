<section class="insidespg-cover py-5" style="background-image: url('<?php echo $this->assetBaseurl2 ?>hero-career.jpg');">
    <div class="prelative container py-5">
        <div class="py-5"></div>
        <div class="row py-5">
            <div class="col-md-30 py-5">
                <div class="insides_intext">
                    <h3 class="mb-2">KARIR</h3>
                    <div class="py-1"></div>
                    <p>Temukan masa depan anda bersama perusahaan nasional yang terus bertumbuh dan mendunia.</p>
                    <div class="clear"></div>
                </div>

            </div>
            <div class="col-md-30"></div>
        </div>
    </div>
</section>

<section class="career-sec-1 py-5 back-white">
    <div class="prelative container py-5">
        <div class="inners content-text text-left">
            <div class="row">
                <div class="col-md-40">
                    <h6>KARIR di EKLIN FERTILIZER GROUP</h6>
                    <p>Berabung bersama EKLIN Fertilizer Group untuk peningkatan karir anda. Anda akan menjadi bagian dari perusahaan global yang berkultur modern dan mampu mendorong anda mencapai performa optimal anda.</p>
                    <p>Informasikan kepada kami, bagaimana anda dapat memberi nilai lebih pada perusahaan. Kirimkan resume anda ke email: <br><a href="mailto:career@eklingroup.com">career@eklingroup.com</a></p>
                </div>
                <div class="col-md-20"></div>
            </div>

            <div class="clear"></div>
        </div>
    </div>
</section>