<section class="insidespg-cover py-5" style="background-image: url('<?php echo $this->assetBaseurl ?>cover-about.jpg');">
    <div class="outers_block_inner">
        <div class="prelative container">
            <div class="row">
                <div class="col-md-60">
                    <div class="insides_intext">
                        <h3 class="mb-2"><?php echo Tt::t('front', 'OUR COMPANY') ?></h3>
                        <div class="py-1"></div>
                        <p><?php echo Tt::t('front', 't_cover_about') ?></p>
                        <div class="clear"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<section class="about-sec-1 py-5 back-white">
    <div class="prelative container py-5">
        <div class="py-3"></div>
        <div class="row text-center py-4">
            <div class="col-md-60 content-text text-center">

                <div class="lgo_smalls d-block justify-content-center text-center mb-3 pb-3">
                    <img src="<?php echo $this->assetBaseurl ?>lgo-cncp-insides-nabout.png" alt="" class="img img-fluid">
                    <div class="py-3"></div>
                    <div class="lines-grey-vertical mx-auto d-block"></div>
                    <div class="py-2"></div>
                </div>
                <?php if (Yii::app()->language == 'en'): ?>
                <h4>WHO WE ARE</h4>
                <h3>Your Woodworking Partner For The Long Run</h3>
                <h5>Built from combined experience of wood processing & wood working and with a production area spanning over 30,000 m2, CNCP is one of the largest wood manufacturer, saw mill & kiln dry facility, and stockholder of semi finished wood products located in Gresik - Surabaya, East Java, Indonesia.</h5>
                <p>CNCP’s wood manufacturing footprint includes the most active saw mill & kiln dry facility in Gresik, Surabaya. The wood processing and manufacturing facilities are supplying world class wood products to support home furnishing, interior decoration, building, architectural & engineering needs at short lead times. CNCP’s monthly capacity exceeds more than 1,650 tons. More than 10 years experience of international wood commodity exports has proven our reliability. Our consistency and punctuality on supply and immediate stock availability are the keys to our success in the international wood export market.</p>
                <?php else: ?>
                <h4>SIAPA KAMI</h4>
                <h3>Mitra Woodworking Anda Untuk Jangka Panjang</h3>
                <h5>Dibangun dari gabungan pengalaman pengolahan kayu & pengerjaan kayu dan dengan area produksi seluas lebih dari 30.000 m2, CNCP adalah salah satu produsen kayu terbesar, fasilitas saw mill & kiln dry, dan pemegang saham produk kayu setengah jadi yang berlokasi di Gresik - Surabaya, Jawa Timur, Indonesia.</h5>
                <p>Jejak produksi kayu CNCP mencakup fasilitas saw mill & kiln dry paling aktif di Gresik, Surabaya. Fasilitas pengolahan dan manufaktur kayu memasok produk kayu kelas dunia untuk mendukung kebutuhan perabotan rumah, dekorasi interior, bangunan, arsitektur & teknik dalam waktu singkat. Kapasitas bulanan CNCP melebihi lebih dari 1.650 ton. Lebih dari 10 tahun pengalaman ekspor komoditas kayu internasional telah membuktikan keandalan kami. Konsistensi dan ketepatan waktu dalam pasokan dan ketersediaan stok segera adalah kunci keberhasilan kami di pasar ekspor kayu internasional.</p>
                <?php endif ?>
            </div>
        </div>
    </div>
</section>



<section class="about-sec-2 py-5 back-white backs_about_pillars">
    <div class="prelative container py-5 text-center">

        <div class="py-2"></div>
        <div class="tops_info_section">
            <?php if (Yii::app()->language == 'en'): ?>                
            <h4 class="small">OUR PILLARS</h4>
            <h3>The Strong Foundation Of CNCP</h3>
            <?php else: ?>
            <h4 class="small">PILAR KAMI</h4>
            <h3>Fondasi Yang Kuat Dari CNCP</h3>
            <?php endif ?>
        </div> 
        <div class="py-3"></div>
        <?php 
        $list_banners = [
                            [
                                'picture'=>'banner-about-pillar-1.jpg',
                                'title'=>'People',
                                'desc'=>'We believe the key to our long term success is the quality and service focus of our people. CNCP wood processing company is committed to strengthen the team through continuous training for skill leverage, staff development to increase accountability and empowerment of our teams throughout the company. With our staff fully dedicated to serving our customers, we look to the future with confidence.',
                                'title_id'=>'Pekerja',
                                'desc_id'=>'Kami percaya bahwa kunci kesuksesan jangka panjang kami adalah kualitas dan fokus layanan dari karyawan kami. Perusahaan pengolahan kayu CNCP berkomitmen untuk memperkuat tim melalui pelatihan berkelanjutan untuk meningkatkan keterampilan, pengembangan staf untuk meningkatkan akuntabilitas dan pemberdayaan tim kami di seluruh perusahaan. Dengan staf kami yang berdedikasi penuh untuk melayani pelanggan kami, kami melihat ke masa depan dengan percaya diri.',
                            ],
                            [
                                'picture'=>'banner-about-pillar-2.jpg',
                                'title'=>'Innovation',
                                'desc'=>'CNCP understands that customer needs are constantly evolving as is the way they want to deal with their ever changing market trends and requests. CNCP will always on the move to innovative new products and services to accommodate the latest trends as clients all over the world will increasingly look for new and practical products they need.',
                                'title_id'=>'Inovasi',
                                'desc_id'=>'CNCP memahami bahwa kebutuhan pelanggan terus berkembang seperti cara mereka ingin menangani tren dan permintaan pasar yang terus berubah. CNCP akan selalu bergerak ke produk dan layanan baru yang inovatif untuk mengakomodasi tren terbaru karena klien di seluruh dunia akan semakin mencari produk baru dan praktis yang mereka butuhkan.',
                            ],
                            [
                                'picture'=>'banner-about-pillar-3.jpg',
                                'title'=>'Efficiency',
                                'desc'=>'The use of processed wood in the world and in Indonesia is increasingly competitive with client’s needs to become even more complex and sophisticated. We are making substantial investments in our wood processing and wood working facility in order to allow us to efficiently meet customer needs and bring out the competitiveness by controling our operating costs.',
                                'title_id'=>'Efisiensi',
                                'desc_id'=>'Penggunaan kayu olahan di dunia dan di Indonesia semakin kompetitif dengan kebutuhan klien yang semakin kompleks dan canggih. Kami melakukan investasi besar dalam pengolahan kayu dan fasilitas pengerjaan kayu untuk memungkinkan kami memenuhi kebutuhan pelanggan secara efisien dan menghadirkan daya saing dengan mengontrol biaya operasional kami.',
                            ],
                            
                        ];
        ?>
        <div class="row lists_pillar_product">
            <?php foreach ($list_banners as $key => $value): ?>
            <div class="col-md-20">
                <div class="items">
                    <div class="pictures">
                        <img src="<?php echo $this->assetBaseurl ?><?php echo $value['picture'] ?>" alt="" class="img img-fluid">
                    </div>
                    <div class="info py-3 pt-4">
                        <?php if (Yii::app()->language == 'en'): ?>
                            <h3><?php echo $value['title'] ?></h3>
                            <p><?php echo $value['desc'] ?></p>
                        <?php else: ?>
                            <h3><?php echo $value['title_id'] ?></h3>
                            <p><?php echo $value['desc_id'] ?></p>
                        <?php endif ?>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <?php endforeach ?>            
        </div>
    </div>
</section>


<section class="about-sec-3 py-5 back-red blocks_outer_vision_mission">
    <div class="prelative container py-5 text-center">
        <div class="inners">
            <div class="tops_info_section">
                <?php if (Yii::app()->language == 'en'): ?>
                    <h4 class="small">CNCP WOOD WORKING & PROCESSING VISION & MISSION</h4>
                <?php else: ?>
                    <h4 class="small">VISI & MISI KERJA & PENGOLAHAN KAYU CNCP</h4>
                <?php endif ?>
            </div> 
            <div class="py-2"></div>
            <div class="lists_bloc_vision_mission">
                <div class="row">
                    <div class="col-md-30 border-r">
                        <div class="texts py-1">
                            <h3><?php echo Tt::t('front', 'Vision') ?></h3>
                            <p><?php echo Tt::t('front', 't_visi') ?></p>
                        </div>
                    </div>
                    <div class="col-md-30">
                        <div class="texts py-1">
                            <h3><?php echo Tt::t('front', 'Mission') ?></h3>
                            <p><?php echo Tt::t('front', 't_misi') ?></p>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</section>