<section class="backs_home1 back-white py-5">
    <div class="prelatife container">
        <div class="inners py-4 text-center">
            <div class="lgo_dx_centers d-block py-3">
                <img src="<?php echo $this->assetBaseurl; ?>logo-middles_pn_cncps.png" alt="" class="img img-fluid mx-auto d-block">
            </div>
            <div class="py-1 my-1"></div>
            <?php if (Yii::app()->language == 'en'): ?>
            <h3>Your Woodworking Partner For The Long Run</h3>
            <div class="py-2 my-1"></div>
            <p>CNCP manufactures various wood products upon customer’s request. All of the resources are from natural wood which sourced with responsibility. CNCP is located at Gresik, Surabaya - East Java and facilitated with the high precision wood manufacturing equipment and machinery, but yet still upholding the tradition of quality checking by our experienced project manager, for a flawless delivery.</p>
            <p><a href="https://youtu.be/09AfJekT9UI" target="_blank" class="views_video">Click here for a quick view of our factory.</a></p>
            <?php else: ?>
            <h3>Mitra Pengerjaan Kayu Anda Untuk Jangka Panjang</h3>
            <div class="py-2 my-1"></div>
            <p>CNCP memproduksi berbagai produk kayu atas permintaan pelanggan. Semua sumber daya tersebut berasal dari kayu alam yang bersumber dari tanggung jawab. CNCP berlokasi di Gresik, Surabaya - Jawa Timur dan difasilitasi dengan peralatan dan mesin manufaktur kayu presisi tinggi, namun tetap menjunjung tradisi pengecekan kualitas oleh manajer proyek kami yang berpengalaman, untuk pengiriman yang sempurna.</p>
            <p><a href="https://youtu.be/09AfJekT9UI" target="_blank" class="views_video">Klik di sini untuk melihat sekilas pabrik kami.</a></p>
            <?php endif ?>
            <div class="clear clearfix"></div>
        </div>
    </div>
</section>

<section class="backs_home2 back-grey py-5">
    <div class="prelatife container">
        <div class="inners py-5 my-4">
            <?php if (Yii::app()->language == 'en'): ?>
            <h5 class="small-titles text-center">CNCP WOOD WORKING COMMITTMENTS</h5>
            <?php else: ?>
            <h5 class="small-titles text-center">KOMITMEN KERJA CNCP WOOD</h5>
            <?php endif ?>

            <div class="py-4 my-2"></div>
            <div class="row default_banner_data">
                <div class="col-md-30 my-auto order-2 order-sm-1">
                    <div class="boxed-content pr-4">
                        <?php if (Yii::app()->language == 'en'): ?>
                        <h3>Committed to <br>
                        <b>Long Term Relationship</b></h3>
                        <p>At CNCP wood manufacturing factory, we respect that wood is a long run commodity, and we aim to build long term relation with our business partners through trust. If the product is consistently high quality and traceable, you will be proud to distribute it. </p>    
                        <?php else: ?>
                        <h3>Berniat untuk<br>
                        <b>Hubungan jangka panjang</b></h3>
                        <p>Di pabrik manufaktur kayu CNCP, kami menghargai bahwa kayu adalah komoditas jangka panjang, dan kami bertujuan untuk membangun hubungan jangka panjang dengan mitra bisnis kami melalui kepercayaan. Jika produk secara konsisten berkualitas tinggi dan dapat dilacak, Anda akan bangga mendistribusikannya.</p>
                        <?php endif ?>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="col-md-30 my-auto order-1 order-sm-2">
                    <div class="picture"><img src="<?php echo $this->assetBaseurl; ?>home-banners-hm-1.jpg" alt="" class="img img-fluid"></div>
                </div>
            </div>

            <div class="py-4 my-3"></div>

            <div class="row default_banner_data">
                <div class="col-md-30 my-auto">
                    <div class="picture"><img src="<?php echo $this->assetBaseurl; ?>home-banners-hm-2.jpg" alt="" class="img img-fluid"></div>
                </div>
                <div class="col-md-30 my-auto">
                    <div class="boxed-content pl-4">
                        <?php if (Yii::app()->language == 'en'): ?>
                            <h3>Committed to <br>
                            <b>Quality Woodworking</b></h3>
                            <p>CNCP wood manufacturing factory is known for the reputation of a precise, strong and consistent wood products. Series of process and treatments with quality control are performed on every step of manufacturing process at CNCP.</p>    
                        <?php else: ?>
                            <h3>Berkomitmen pada <br>
                            <b>Pengerjaan Kayu Berkualitas</b></h3>
                            <p>Pabrik pembuatan kayu CNCP dikenal dengan reputasi produk kayu yang presisi, kuat dan konsisten. Rangkaian proses dan perawatan dengan kendali mutu dilakukan pada setiap tahapan proses manufaktur di CNCP.</p>
                        <?php endif ?>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>

            <div class="clear clearfix"></div>
        </div>
    </div>
</section>

<section class="backs_home3 back-brown py-5">
    <div class="prelatife container">
        <div class="inners py-4 text-center">
            <?php if (Yii::app()->language == 'en'): ?>
            <h5 class="small-titles">CNCP WOOD WORKING SERVICES RANGE</h5>
            <?php else: ?>
            <h5 class="small-titles">JASA PENGERJAAN KAYU CNCP</h5>
            <?php endif ?>

            <div class="py-4"></div>
            <?php 
            $bc_wood = [
                        [
                            'picture'=>'banner-ft-sn-1.jpg',
                            'title'=>'Saw Mill',
                        ],
                        [
                            'picture'=>'banner-ft-sn-2.jpg',
                            'title'=>'Wood Working',
                        ],
                        [
                            'picture'=>'banner-ft-sn-3.jpg',
                            'title'=>'Kiln Dry',
                        ],
                        [
                            'picture'=>'banner-ft-sn-4.jpg',
                            'title'=>'Export Commodity',
                        ],
                       ];
            ?>
            <div class="lists_block_woodworking">
                <div class="row">
                    <?php foreach ($bc_wood as $key => $value): ?>
                    <div class="col-md-15 col-30">
                        <div class="boxed_data prelatife">
                            <img src="<?php echo $this->assetBaseurl. $value['picture']; ?>" alt="" class="img img-fluid">
                            <div class="texts"><span><?php echo $value['title'] ?></span></div>
                        </div>
                    </div>
                    <?php endforeach ?>
                </div>
            </div>
            <div class="py-1 my-1"></div>

            <div class="clear"></div>
        </div>
    </div>
</section>